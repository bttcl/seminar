# include <stdlib.h>
# include <stdio.h>
# include "aux.h"

# ifndef N_EVENTS
# define N_EVENTS 1e3
# endif

int
main(int argc, char * argv[]) {
    unsigned long int eventNumber = N_EVENTS;
    Byte i, value, histogram[256];
    FILE * outFile;

    /* It is always a good idea, to inform user, if something goes wrong. */
    if( 3 != argc ||
        !('t' == argv[1][0] || 'g' == argv[1][0] || 'p' == argv[1][0]) ) {
        message( "Syntax is:\n\t$ " );
        message( argv[0] );
        message( " <t/g/p> <filename/-->\n" );
        return 1;  /* Application returns a non-null result, indicating that
                      something went wrong. */
    }

    /* Initialize each histogramm's bin with zero. */
    for( i = 0; i < 255; ++i ) {
        histogram[i] = 0;
    }

    /* Now, do the generation. It is another good idea, to report the user
     * about continious operations. */
    message( "Generating stuff...\n" );
    do {
        /* Please, mention that we loose some processor time here for
         * checking some logical conditions that can never be changed
         * inside a loop by algorithm conditions. We can avoid this lack
         * by using callbacks (we talk about it later). */
        if(        't' == argv[1][0] ) {
            value = generate_triagonal();
        } else if( 'g' == argv[1][0] ) {
            value = generate_gaussian();
        } else if( 'p' == argv[1][0] ) {
            value = generate_poisson();
        } else {
            message("Internal logic error.\n");
            return 2;
        }
        /* Common section: */
        ++histogram[value];
    } while( eventNumber-- );

    /* Write the histogram just as plain file. */
    message( "Saving data to \"" );
    if( argv[2][0] == argv[2][1] &&
        '-' == argv[2][1]) {
        outFile = stdout;
    } else {
        outFile = fopen( argv[2], "w" );
    }
    message( argv[2] );
    message( "\"...\n" );
    /* First of all, acquire file descriptor that we are supposed to write to. */
    {  /* <- those brackets here if for your
                                         * own convinience only. */
        for( i = 0; i < 255; ++i ) {
            print_byte_decimal_to( outFile, *(histogram+i) /*histogram[i]*/ );
        }
    } fclose( outFile );  /* Here we tell the kernel to unbind file descriptor. */

    /* We're all done here. */
    message( "Events generated and written. Bye.\n" );
    return 0;
}

