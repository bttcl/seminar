# ifndef H_248SEMINAR_AUX_H
# define H_248SEMINAR_AUX_H

typedef unsigned char Byte;
typedef Byte* ByteArray;

/** Returns a pseudorandom number with triagonal propability densitiy [0-255]
  * centered at 128. */
Byte generate_triagonal();
/** Returns a pseudorandom number according Poisson distribution law (TODO: params). */
Byte generate_poisson();
/** Returns a pseudorandom number with Gaussian law wtih parameters (TODO: params). */
Byte generate_gaussian();


/*
 * TODO move it outta here:
 */

/** Prints a provided message string to terminal's standard output. */
void message( const char * );

/** Print decimal number by given file descriptor. */
void print_byte_decimal_to( void *, Byte );

# endif  /* H_248SEMINAR_AUX_H */

