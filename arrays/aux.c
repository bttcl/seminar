
# include <stdio.h>  /* for putchar or putc */
# include <math.h>   /* for sqrt, M_PI */
# include <stdlib.h> /* for rand() */
# include "aux.h"

/* TODO: move it outta here: */
void
message( const char * msg ) {
    const char * c = msg;
    do {
        putchar(*c);
    } while( *(++c) );
}


void
print_byte_decimal_to( void * fd_, Byte what ) {
    FILE * f = (FILE *) fd_;
    fprintf( f, "%d\n", (int) what );
}

/*
 * Generators
 */

Byte
generate_triagonal() {
    /* see: http://www.sciencedirect.com/science/article/pii/S0895717708002665 */
    double u = rand()/(double)(RAND_MAX),
           c = .5; /* average */
    return (u < c ? sqrt(c*u) : 1 - sqrt( (1-c)*(1-u) ))*255;
}

Byte
generate_poisson() {
    /* see: http://en.wikipedia.org/wiki/Poisson_distribution#Generating_Poisson-distributed_random_variables */
    double lambda = 4,        /* average */
           L = exp(-lambda),
           p = 1.;
    Byte k = 0;
    do {
        ++k;
        p *= rand()/(double)(RAND_MAX);
    } while( p > L );
    return k -= 1;
}

Byte
generate_gaussian() {
    /* see http://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform */
    double u = rand()/(double)(RAND_MAX),
           v = rand()/(double)(RAND_MAX);
    return (sqrt(-2. * log(u))*sin(2*M_PI*v))*30 + 128;
}

