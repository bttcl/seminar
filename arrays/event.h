/* Header file declaring common types that are used by several
 * source files. Header files `shares' type and functions
 * declarations between several source files and can provide
 * visibility to any routine that need those declaration at any
 * time during development.
 * */

# ifndef H_SEMINAR_STRUCT_EVENT_H /* sentinel */
# define H_SEMINAR_STRUCT_EVENT_H

typedef struct Event {
    unsigned int timestamp;
           float magnitude;
} Event;

# endif  /* H_SEMINAR_STRUCT_EVENT_H */

