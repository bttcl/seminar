import math;

#Normalized only
class IEEE_float(object):
    def __init__(self, sign, exponent, mantissa):
        self.sign = int(sign);
        self.exponent = exponent;
        self.mantissa = "1" + mantissa; #quiet one

    def __to_dec(self, binary, exponent):
        result = 0.0;
        for c in binary:
            result += int(c) * (2 ** exponent)
            exponent -= 1
        return result;


    def exp(self):
        return self.__to_dec(self.exponent, 7) - 127;

    def val(self):
        return self.__to_dec(self.mantissa, self.exp());



def binary(value):
    """
    Prints value as sum of 2^x
    """
    print "%d = " % value,
    while True:
        exp = math.floor(math.log(value, 2));
        value -= 2 ** exp
        print "2^%d" % exp,

        if value <= 0:
            break;

        print " +",

    print


def main():
    a = IEEE_float("0", "10011001", "11010110111100110100011");
    b = IEEE_float("0", "10011001", "11010110111100110100010");

    print(a.val());
    print(b.val());
    print (a.val() - b.val());


if __name__ == '__main__':
    main()