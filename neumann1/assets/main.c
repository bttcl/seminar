# include <stdio.h>
# include <stdlib.h>
# include <strings.h>
# include <math.h>

# include <gsl_cdf.h>

// Define this to print ALL points with in/out prefix:
//# define DUMP_NEUMANN_TRIGGERING

float _f(float x) {
    return 1/(x*x + 1);
}

float roll() {
    float a,b;
    do {
        a = random()/( (float) RAND_MAX );
        b = random()/( (float) RAND_MAX );
        # ifdef DUMP_NEUMANN_TRIGGERING
        if( a > _f(b) ) {
            printf( "out: %f %f\n", a, b );
        } else {
            printf( " in: %f %f\n", a, b );
        }
        # endif
    } while( a > _f(b));
    return b;
}

int main(int argc, char * argv[]) {
    int i;
    float value, dp, ddp, delta, x, chi2 = 0;
    int counts[10];
    const int n = atoi(argv[1]);
    const int nBins = sizeof(counts)/sizeof(int);
    bzero( counts, sizeof(counts) );
    for( i = 0; i < n; i++ ) {
        value = roll();
        counts[(int) (value*10)] ++;
    }
    for( i = 0; i < nBins; ++i ) {
        x = (i+.5)/((float) nBins);
        ddp = counts[i]/((float) n);
        dp = (atan((float)(i+1)/nBins) - atan((float)i/nBins))/(M_PI/4);
        delta = fabs(dp - ddp);
        printf( "%f %f %f %e\n", x, dp, ddp, delta/dp );
        chi2 += delta*delta/dp;
    }
    chi2 *= nBins - 1;
    printf( "::chi2 = %e\n", chi2 );
    printf( "::p-value = %e\n", gsl_cdf_chisq_P(chi2, nBins - 1) );
    return 0;
}
