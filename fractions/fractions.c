# include <stdio.h>
# include "fractions.h"

struct Fraction
sum(      struct Fraction a, struct Fraction b ) {
    struct Fraction res;
    res.nominator = a.nominator*b.denominator +
                    b.nominator*a.denominator;
    res.denominator = a.denominator*b.denominator;
    return res;
}

struct Fraction
subtract( struct Fraction a, struct Fraction b ) {
    struct Fraction res;
    res.nominator = a.nominator*b.denominator -
                    b.nominator*a.denominator;
    res.denominator = a.denominator*b.denominator;
    return res;
}

struct Fraction
prod(     struct Fraction a, struct Fraction b ) {
    struct Fraction res;
    res.nominator   = a.nominator*b.nominator;
    res.denominator = a.denominator*b.denominator;
    return res;
}

void
print_fraction(struct Fraction a) {
    printf( "%u/%u\n", a.nominator, a.denominator );
}

struct Fraction
eval( struct Fraction(*f)(struct Fraction, struct Fraction),
        struct Fraction a,
        struct Fraction b ) {
    return f(a, b);
}

