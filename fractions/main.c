# include "fractions.h"

int
main(int argc, char * argv[]) {
    struct Fraction(*toEval)(struct Fraction, struct Fraction) = 0;
    struct Fraction c;
    struct Fraction a = {1, 3},
                    b = {2, 3};

    if( 2 == argc ) {
        if(         '-' == argv[1][0] ) {
            toEval = subtract;
        } else if(  '+' == argv[1][0] ) {
            toEval = sum;
        } else if(  '*' == argv[1][0] ) {
            toEval = prod;
        }
    }

    c = eval( toEval, b, a );

    print_fraction(c);

    return 0;
}

