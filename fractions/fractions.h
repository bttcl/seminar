
typedef unsigned short Number;

struct Fraction {
    Number nominator;
    Number denominator;
};

struct Fraction sum(      struct Fraction, struct Fraction );
struct Fraction subtract( struct Fraction, struct Fraction );
struct Fraction prod(     struct Fraction, struct Fraction );

void print_fraction(struct Fraction);
struct Fraction eval( struct Fraction(*)(struct Fraction, struct Fraction),
                      struct Fraction,
                      struct Fraction );

