/**
 * Envy (envy@qcrypt.org)
 *
 * Pure C program, which writes HEX value of given input
 * */

#include <stdio.h>
typedef unsigned char Byte;


void print_bin(Byte value) {
    size_t i = (sizeof(Byte) * 8) - 1;
    Byte shift = 1;

    putchar('b');

    do {
        if ( value & ( shift << i ) ) {
            putchar('1');
        } else {
            putchar('0');
        }
    } while ( i-- );

    putchar('\n');
}

void print_hex(Byte value) {
    size_t length = sizeof(value) * 2; /* Since 1 byte = 2 HEX chars */
    size_t i = 0;
    char offset;

    putchar('0'); putchar('x');

    while ( i++ < length ) {
        unsigned char _temp = ((value >> ((length - i) * 4)) & 0x0f );
        offset = '0';
        if ( _temp > 9 ) {
            _temp -= 10;
            offset = 'A';
        }
        putchar(offset + _temp);
    };

    putchar('\n');
}


int main(int argc, char* argv[]) {
    /*
     * NOTICE IT!
     * This i tried to check my algorithm by using formatted print,
     * and it ends with GIANT fuckup
     *
     * $./a.out > table
     * creates 3Gb file
     *
     *
     * Question: what i did wrong? )
    for ( Byte i = 0; i <= 0xff; ++i ) {
        print_hex(i);
        printf("%#02x\n\n", i);
    }
    */

    Byte val = 0xfa;
    print_bin(val);
    print_hex(val);
}
