# include <stdlib.h>
# include <stdio.h>

static float coefficients[16];

int
main(int argc, char * argv[]) {
    if(argc != 2) {
        fprintf( stderr, "Syntax is\n$ %s <filename>\n",
                 argv[0] );
        return EXIT_FAILURE;
    }

    const char * filename;
    filename = argv[1];

    FILE * inFile = fopen( filename, "r" );
    if( !inFile ) {
        fprintf(stderr, "Couldn't open file \"%s\".\n", filename );
        return EXIT_FAILURE;
    }
    size_t i;
    for( i = 0;
         EOF != scanf(/*inFile,*/ "%f\n", coefficients + i);
         ++i ) {
        //printf( "read: %f\n", coefficients + i );  // XXX
    }
    fclose( inFile );
    fprintf( stdout, "Read %zd entries.\n", i );

    return EXIT_SUCCESS;
}

