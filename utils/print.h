/*
 * No documentation needed, i guess.
 *
 * This software provided "AS IS", and the author disclaims
 * all warranties and blah-blah-blah...
 * You can take this code, and do whatever you want.
 *
 * Envy (envy@qcrypt.org)
 *
 * */

#ifndef _UTIL_PRINT_H
#define _UTIL_PRINT_H 1

#include <stdio.h>

/* UBER FUNCTION OVERLOADING */
#define bin(X) print_bin(&X, sizeof(X))

void print_byte(unsigned char byte) {
    unsigned char i = 7;

    do {
        if ( byte & (1 << i) ) {
            putchar('1');
        } else {
            putchar('0');
        }
    } while(i--);
}



void print_bin(void const * const ptr, int const size) {
    int i;
    unsigned char *b = (unsigned char*) ptr;

    putchar('b'); putchar(' ');
    for ( i = size - 1; i >= 0; i-- ) {
        print_byte(b[i]);
        putchar(' ');
    }

    int maxsize = 8;
    int pad = (maxsize - size) * 9;

    if( pad > 0 ) {
       pad += 1;
    }

    printf(" %*c%d bytes, %2d bits)\n", pad, '(', size, size*8);
}

#endif
