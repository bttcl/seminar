# include <stdlib.h>
# include <math.h>
# include "event.h"

int
main(int argc, char * argv[]) {
    struct Event a;
    a.timestamp = 123;
    a.magnitude = 1.23;

    print_event( &a );
    print_event( &a );

    return EXIT_SUCCESS;
}

