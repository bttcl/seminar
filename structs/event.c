# include <stdio.h>
# include "event.h"

void
print_event( struct Event * e ) {
    printf( "time: %u, magnitude: %f\n",
            (*e).timestamp,
            (*e).magnitude );
}

